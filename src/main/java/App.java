import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

public class App {
    public static void main(String[] args) throws InvocationTargetException, InterruptedException {
//        File file = new File("3D MR Neck.dcm");
        File file = new File("EnhancedCT_Anon.dcm");
        try (DicomReader reader = new DicomReader(file)) {
            DicomBuffer buffer = reader.getDataAsBuffer();

            Renderer renderer = new Renderer(buffer);

            Gui gui = new Gui(reader, renderer);
            SwingUtilities.invokeAndWait(gui);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
