import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Buttons;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector3;

/**
 * Manipulate a Matrix4 transformation-matrix using mouse and keyboard.
 * Mouse-drag/A-D: Rotate
 * Scroll: Zoom
 */
public class ObjectManipulationInputController extends InputAdapter {
    private final int rotateButton = Buttons.LEFT;
    private final int rotateRightKey = Keys.A;
    private final int rotateLeftKey = Keys.D;

    private final float rotateAngle = 360f;

    private boolean rotateRightPressed;
    private boolean rotateLeftPressed;
    private int button = -1;

    private float startX, startY;

    public Matrix4 getTransformationMatrix() {
        return transformationMatrix;
    }

    private final Matrix4 transformationMatrix;
    private final Vector3 up = new Vector3(0, 1, 0);

    protected ObjectManipulationInputController() {
        this.transformationMatrix = new Matrix4().idt();
    }

    public void update() {
        if (rotateRightPressed || rotateLeftPressed) {
            final float delta = Gdx.graphics.getDeltaTime();
            if (rotateRightPressed) {
                transformationMatrix.rotate(up, -delta * rotateAngle);
                up.rotate(up, -delta * rotateAngle);
            }
            if (rotateLeftPressed) {
                transformationMatrix.rotate(up, delta * rotateAngle);
                up.rotate(up, delta * rotateAngle);
            }
        }
    }

    private int touched;
    private boolean multiTouch;

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        touched |= (1 << pointer);
        multiTouch = !MathUtils.isPowerOfTwo(touched);
        if (multiTouch)
            this.button = -1;
        else if (this.button < 0) {
            startX = screenX;
            startY = screenY;
            this.button = button;
        }
        return super.touchDown(screenX, screenY, pointer, button);
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        touched &= -1 ^ (1 << pointer);
        multiTouch = !MathUtils.isPowerOfTwo(touched);
        if (button == this.button) this.button = -1;
        return super.touchUp(screenX, screenY, pointer, button);
    }

    protected boolean process(float deltaX, float deltaY, int button) {
        if (button == rotateButton) {
            transformationMatrix.rotate(new Vector3(1, 0, 0), deltaY * rotateAngle);
            transformationMatrix.rotate(new Vector3(0, 1, 0), deltaX * rotateAngle);
        }
        return true;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        boolean result = super.touchDragged(screenX, screenY, pointer);
        if (result || this.button < 0) return result;
        final float deltaX = (screenX - startX) / Gdx.graphics.getWidth();
        final float deltaY = (startY - screenY) / Gdx.graphics.getHeight();
        startX = screenX;
        startY = screenY;
        return process(deltaX, deltaY, button);
    }

    @Override
    public boolean scrolled(float amountX, float amountY) {
        if (amountY < 0) {
            return zoom(0.9f);
        } else {
            return zoom(1.1f);
        }

    }

    public boolean zoom(float amount) {
        transformationMatrix.scale(amount, amount, amount);
        return true;
    }

    @Override
    public boolean keyDown(int keycode) {
        if (keycode == rotateRightKey)
            rotateRightPressed = true;
        else if (keycode == rotateLeftKey) rotateLeftPressed = true;
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        if (keycode == rotateRightKey)
            rotateRightPressed = false;
        else if (keycode == rotateLeftKey) rotateLeftPressed = false;
        return false;
    }
}
