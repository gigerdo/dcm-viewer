import com.badlogic.gdx.backends.lwjgl.LwjglAWTCanvas;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;

public class Gui implements Runnable {
    private final DicomReader reader;
    private final Renderer renderer;

    public Gui(DicomReader reader, Renderer renderer) {
        this.reader = reader;
        this.renderer = renderer;
    }

    public void run() {
        JFrame frame = new JFrame("DICOM Viewer");
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                // Swing/libgdx cause to deadlock each other, force kill app
                Runtime.getRuntime().halt(0);
            }
        });
        frame.getContentPane().setLayout(new BorderLayout());


        JSplitPane splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
        splitPane.setDividerSize(4);
        frame.getContentPane().add(splitPane);

        JPanel spacer = new JPanel(new BorderLayout());
        splitPane.add(spacer, JSplitPane.BOTTOM);

        LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        LwjglAWTCanvas canvas = new LwjglAWTCanvas(renderer, config);
        spacer.add(canvas.getCanvas());

        JSlider threshold = new JSlider(JSlider.HORIZONTAL, 0, 1000, 0);
        threshold.addChangeListener(e -> {
            renderer.setThreshold(((JSlider) e.getSource()).getValue() / 1000f);
        });
        spacer.add(threshold, BorderLayout.SOUTH);

        JPanel panel = new JPanel();
        panel.setLayout(new FlowLayout());
        for (int i = 0; i < reader.getNumImages(); i++) {
            JLabel label = new JLabel();
            BufferedImage image = reader.getImage(i);
            Image scaledImage = image;
            if (image.getHeight() > 512) {
                float aspectRatio = (float) image.getWidth() / image.getHeight();
                scaledImage = image.getScaledInstance((int) (512 * aspectRatio), 512, Image.SCALE_DEFAULT);
            }
            label.setIcon(new ImageIcon(scaledImage));
            panel.add(label);
        }
        JScrollPane scroll = new JScrollPane(panel);
        scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
        splitPane.add(scroll, JSplitPane.TOP);


        frame.pack();
        frame.setVisible(true);
        frame.setSize(800, 800);
    }
}
