import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Matrix4;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;
import org.lwjgl.opengl.GL13;

import static org.lwjgl.opengl.EXTBlendMinmax.GL_MAX_EXT;
import static org.lwjgl.opengl.EXTBlendMinmax.glBlendEquationEXT;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL12.glTexImage3D;

public class Renderer implements ApplicationListener {
    private final DicomBuffer buffer;
    private float threshold = 0.0f;

    private Camera cam;
    private ShaderProgram shaderProgram;
    private Mesh mesh;
    private int volumeTextureHandle;
    private ObjectManipulationInputController inputController;
    private Matrix4 spacingTransform;

    public Renderer(DicomBuffer buffer) {
        this.buffer = buffer;
    }

    @Override
    public void create() {
        cam = new PerspectiveCamera(45, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        cam.position.set(0.f, 0.f, 2f);
        cam.lookAt(0.f, 0.f, 0);
        cam.near = 0.1f;
        cam.update();

        loadDicomData();

        // Build mesh (n slices that the 3d-texture will be rendered on)
        int n = 1000;
        mesh = new Mesh(true, n * 4, 0,
                new VertexAttribute(VertexAttributes.Usage.Position, 3, "position")
        );
        float[] vertices = new float[n * 4 * 3];
        float zd = 1.0f / n;
        for (int i = 0; i < n; i += 2) {
            float z = i * zd;
            float[] floats = {
                    0.0f, 1.0f, z,
                    0.0f, 0.0f, z,
                    1.0f, 1.0f, z,
                    1.0f, 0.0f, z,

                    1.0f, 0.0f, z + zd,
                    1.0f, 1.0f, z + zd,
                    0.0f, 0.0f, z + zd,
                    0.0f, 1.0f, z + zd,
            };

            int idx = i * 4 * 3;
            System.arraycopy(floats, 0, vertices, idx, floats.length);
        }
        mesh.setVertices(vertices);
        mesh.transform(
                new Matrix4().idt()
                        .translate(-4f, -4f, -4f)
                        .scale(8f, 8f, 8f)
        );

        shaderProgram = new ShaderProgram(Gdx.files.internal("vertex.glsl"), Gdx.files.internal("fragment.glsl"));
        ShaderProgram.pedantic = false;
        System.out.println(shaderProgram.getLog());

        float[] spacing = buffer.getSpacing();
        spacingTransform = new Matrix4().idt()
                .scale(spacing[0], spacing[1], spacing[2]);

        glEnable(GL_BLEND);
//        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glBlendFunc(GL_ONE, GL_ONE);
        glBlendEquationEXT(GL_MAX_EXT);

        // Set up inputs
        inputController = new ObjectManipulationInputController();
        Gdx.input.setInputProcessor(inputController);
    }

    @Override
    public void resize(int width, int height) {
        Gdx.gl.glViewport(0, 0, width, height);
        cam.viewportWidth = width;
        cam.viewportHeight = height;
        cam.update();
    }

    @Override
    public void render() {
        inputController.update();

        Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        Gdx.gl20.glClearColor(0.f, 0.f, 0.f, 1);
        Gdx.gl20.glClear(GL20.GL_COLOR_BUFFER_BIT);

        shaderProgram.bind();
        Gdx.gl.glActiveTexture(GL20.GL_TEXTURE0);
        Gdx.gl.glBindTexture(GL30.GL_TEXTURE_3D, volumeTextureHandle);
        shaderProgram.setUniformi("volumeTexture", 0);

        shaderProgram.setUniformMatrix("cameraTransform", cam.combined);
        shaderProgram.setUniformMatrix("volumeTextureTransform", inputController.getTransformationMatrix());

        shaderProgram.setUniformMatrix("spacingTransform", spacingTransform);

        shaderProgram.setUniformf("threshold", threshold);

        mesh.render(shaderProgram, GL30.GL_TRIANGLE_STRIP);
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void dispose() {
        mesh.dispose();
        shaderProgram.dispose();
        glDeleteTextures(volumeTextureHandle);
    }

    private void loadDicomData() {
        Gdx.gl.glEnable(GL12.GL_TEXTURE_3D);
        volumeTextureHandle = glGenTextures();
        glBindTexture(GL12.GL_TEXTURE_3D, volumeTextureHandle);
        Gdx.gl.glPixelStorei(GL20.GL_UNPACK_ALIGNMENT, 1);
        glTexImage3D(GL12.GL_TEXTURE_3D, 0, GL11.GL_RED, buffer.getWidth(), buffer.getHeight(), buffer.getDepth(),
                0, GL11.GL_RED, GL_UNSIGNED_BYTE, buffer.getByteBuffer());
        Gdx.gl.glTexParameteri(GL12.GL_TEXTURE_3D, GL11.GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        Gdx.gl.glTexParameteri(GL12.GL_TEXTURE_3D, GL11.GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        Gdx.gl.glTexParameteri(GL12.GL_TEXTURE_3D, GL11.GL_TEXTURE_WRAP_S, GL13.GL_CLAMP_TO_BORDER);
        Gdx.gl.glTexParameteri(GL12.GL_TEXTURE_3D, GL11.GL_TEXTURE_WRAP_T, GL13.GL_CLAMP_TO_BORDER);
        Gdx.gl.glTexParameteri(GL12.GL_TEXTURE_3D, GL12.GL_TEXTURE_WRAP_R, GL13.GL_CLAMP_TO_BORDER);
        glBindTexture(GL12.GL_TEXTURE_3D, 0);
    }

    private void load2dTexture() {
        volumeTextureHandle = glGenTextures();
        glBindTexture(GL11.GL_TEXTURE_2D, volumeTextureHandle);
        TextureData textureData = TextureData.Factory.loadFromFile(Gdx.files.internal("badlogic.jpg"), null, false);
        if (!textureData.isPrepared()) textureData.prepare();
        Pixmap pixmap = textureData.consumePixmap();
        Gdx.gl.glPixelStorei(GL20.GL_UNPACK_ALIGNMENT, 1);
        glTexImage2D(GL11.GL_TEXTURE_2D, 0, pixmap.getGLInternalFormat(), pixmap.getWidth(), pixmap.getHeight(), 0, pixmap.getGLFormat(), pixmap.getGLType(), pixmap.getPixels());
        Gdx.gl.glTexParameteri(GL11.GL_TEXTURE_2D, GL20.GL_TEXTURE_MIN_FILTER, Texture.TextureFilter.Nearest.getGLEnum());
        Gdx.gl.glTexParameteri(GL11.GL_TEXTURE_2D, GL20.GL_TEXTURE_MAG_FILTER, Texture.TextureFilter.Nearest.getGLEnum());
        glBindTexture(GL11.GL_TEXTURE_2D, 0);
        pixmap.dispose();
    }

    public void setThreshold(float v) {
        this.threshold = v;
    }
}
