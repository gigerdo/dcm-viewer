/**
 * Real-world space between pixels in mm.
 */
public class VolumeSpacing {
    private final float x;
    private final float y;
    private final float z;

    public VolumeSpacing(float xSpacing, float ySpacing, float zSpacing) {
        this.x = xSpacing;
        this.y = ySpacing;
        this.z = zSpacing;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public float getZ() {
        return z;
    }

    @Override
    public String toString() {
        return "VolumeSpacing{" +
                "x=" + x +
                ", y=" + y +
                ", z=" + z +
                '}';
    }
}
