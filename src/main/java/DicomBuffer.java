import java.nio.ByteBuffer;

public class DicomBuffer {
    private final int width;
    private final int height;
    private final int depth;
    private final ByteBuffer buffer;

    private final float widthMm;
    private final float heightMm;
    private final float depthMm;

    public DicomBuffer(int width, int height, int depth, ByteBuffer buffer, VolumeSpacing spacing) {
        this.width = width;
        this.height = height;
        this.depth = depth;
        this.buffer = buffer;

        widthMm = spacing.getX() * width;
        heightMm = spacing.getY() * height;
        depthMm = spacing.getZ() * depth;

        System.out.printf("Format: %fmm x %fmm x %fmm%n", widthMm, heightMm, depthMm);
        System.out.printf("Factor: %fmm x %fmm x %fmm%n", 1.0f, widthMm / heightMm, widthMm / depthMm);
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getDepth() {
        return depth;
    }

    public ByteBuffer getByteBuffer() {
        return buffer.asReadOnlyBuffer();
    }

    public float[] getSpacing() {
        return new float[]{
                1.0f,
                widthMm / heightMm,
                widthMm / depthMm,
        };
    }
}
