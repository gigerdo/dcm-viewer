import org.dcm4che3.data.Attributes;
import org.dcm4che3.data.ItemPointer;
import org.dcm4che3.data.Sequence;
import org.dcm4che3.data.Tag;
import org.dcm4che3.imageio.plugins.dcm.DicomImageReader;
import org.dcm4che3.imageio.plugins.dcm.DicomMetaData;

import javax.imageio.ImageIO;
import javax.imageio.stream.FileImageInputStream;
import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.stream.IntStream;

public class DicomReader implements AutoCloseable {
    private final DicomImageReader reader;
    private final FileImageInputStream fileStream;
    private final DicomMetaData metaData;

    public DicomReader(File file) {
        try {
            reader = (DicomImageReader) ImageIO.getImageReadersByFormatName("DICOM").next();
            fileStream = new FileImageInputStream(file);
            reader.setInput(fileStream);
            metaData = reader.getStreamMetadata();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public int getNumImages() {
        try {
            return reader.getNumImages(true);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public BufferedImage getImage(int i) {
        try {
            return reader.read(i);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void save() {
        // TODO: Make it work
//        Attributes attributes = new Attributes();
//        Sequence seq = attributes.newSequence(Tag.GraphicAnnotationSequence, 1);
//        Attributes line = new Attributes();
//        line.newSequence(Tag.LineSequence, 1);
//        seq.add(line);
//        try(DicomOutputStream dos = new DicomOutputStream(new File("newFile.dcm"))) {
//            dos.writeDataset(reader);
//        }  catch (IOException e) {
//            throw new RuntimeException(e);
//        }
    }

    @Override
    public void close() throws IOException {
        reader.dispose();
        fileStream.close();
    }

    public int getWidth() {
        try {
            return reader.getWidth(0);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public int getHeight() {
        try {
            return reader.getHeight(0);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public DicomBuffer getDataAsBuffer() {
        int width = getWidth();
        int height = getHeight();
        int depth = getNumImages();
        VolumeSpacing spacing = getSpacing();

        System.out.println(spacing.toString());
        System.out.printf("Format: %dx%dx%d%n", width, height, depth);

        ByteBuffer buffer = ByteBuffer.allocateDirect(width * height * depth);
        IntStream.range(0, depth)
                .mapToObj(this::getImage)
                .map(image -> {
                    DataBuffer dataBuffer = image.getRaster().getDataBuffer();
                    if (dataBuffer instanceof DataBufferByte) {
                        return ByteBuffer.wrap(((DataBufferByte) dataBuffer).getData());
                    } else {
                        throw new RuntimeException("Unknown databuffer type");
                    }
                })
                .forEach(buffer::put);
        buffer.rewind();

        return new DicomBuffer(width, height, depth, buffer, spacing);
    }

    public VolumeSpacing getSpacing() {
        Attributes sharedFunctionalGroups = metaData.getAttributes().getNestedDataset(
                new ItemPointer(Tag.SharedFunctionalGroupsSequence, 0),
                new ItemPointer(Tag.PixelMeasuresSequence, 0)
        );

        if (sharedFunctionalGroups == null) {
            return new VolumeSpacing(1.0f, 1.0f, 1.0f);
        }

        String[] pixelSpacing = sharedFunctionalGroups
                .getStrings(Tag.PixelSpacing);

        float x = Float.parseFloat(pixelSpacing[0]);
        float y = Float.parseFloat(pixelSpacing[1]);


        Sequence sequence = metaData.getAttributes().getSequence(Tag.PerFrameFunctionalGroupsSequence);

        ArrayList<Float> planePositionZ = new ArrayList<>();
        for (Attributes attributes : sequence) {
            float[] imagePositionPatient = attributes.getNestedDataset(Tag.PlanePositionSequence).getFloats(Tag.ImagePositionPatient);
            planePositionZ.add(imagePositionPatient[2]); // Add z position
        }

        float sliceThickness = Math.abs(planePositionZ.get(0) - planePositionZ.get(planePositionZ.size() - 1));
        float z = sliceThickness / getNumImages();

        return new VolumeSpacing(x, y, z);
    }
}
