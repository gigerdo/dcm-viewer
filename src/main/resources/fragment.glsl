#version 150

#ifdef GL_ES
precision mediump float;
#endif

uniform sampler3D volumeTexture;
uniform float threshold;
uniform mat4 spacingTransform;

in vec4 volumeTexCoord;
out vec4 color;

void main() {
    vec3 pos = (spacingTransform * volumeTexCoord).xyz + vec3(0.5, 0.5, 0.5);
    vec4 grey = texture(volumeTexture, pos);

    float alpha = grey.r;
    if (alpha < threshold) {
        alpha = 0.0;
    }
    color = vec4(alpha, alpha, alpha, alpha);
}