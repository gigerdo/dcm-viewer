#version 150

uniform mat4 cameraTransform;
uniform mat4 volumeTextureTransform;

in vec4 position;
out vec4 volumeTexCoord;

void main() {
    volumeTexCoord = volumeTextureTransform * position;
    gl_Position = cameraTransform * position;
}