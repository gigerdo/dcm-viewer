# DCM-Viewer

Simple DICOM (.dcm) Viewer. Shows individual frames and creates volumetric 3d representation from all frames.

Using:

- dcm4che to read Dicom
- libGDX, lwjgl to render the volume

## Run

`gradlew run`

# TODO

- Scale rendering according to scale information from DICOM (SliceThickness, PixelSpacing)
- Visualize region of interest (ROI)
- Mark ROI on 2D view
- Mark ROI on 3D view
- Update dcm with new ROI.